\include "articulate.ly"
\include "../include/global.ly"
\include "../include/harmony.ly"
\include "../include/outline.ly"
\include "../include/bass.ly"

\score {
	\unfoldRepeats \articulate
	\new StaffGroup <<
		\new ChordNames {
			\harmony
		}
		\new Staff <<
			\set Staff.midiInstrument = "baritone sax"
			\global \outline \bass
		>>
	>>
	\midi {}
}
