outline = {
	\override DynamicLineSpanner #'staff-padding = #2

	%% A %%
	s1*4^\markup {\tiny \bold "Intro" }
	\bar "||"
	%% B %%
	s1*4
	\bar "|."
}
