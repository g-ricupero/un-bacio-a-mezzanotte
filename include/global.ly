\version "2.16.2"

\paper {
	#(set-paper-size "a4")          % paper size
	% ragged-last-bottom = ##f      % fill the page till bottom
	% page-count = 1                % force to render just in 1 page
	% between-system-padding = 0    % empty padding between lines
}

NoChords = {
	\override Score.MetronomeMark #'padding = #4
}
WithChords = {
	\override Score.MetronomeMark #'padding = #8
}

\header {
	title = \markup \center-align {
		\override #'(font-name . "Purisa")
		\fontsize #4 \bold
		"Un Bacio a Mezzanotte"
	}
	subsubtitle = \markup \center-align {
		\override #'(font-name . "Arial")
		"Spaghetti Style Version"
	}
	composer=\markup \center-align {
		\override #'(font-name . "Arial")
		"Garinei, Giovannini, Kramer"
	}
	opus = \markup \tiny {
		\override #'(font-name . "Arial")
		"(1952)"
	}
	tagline = \markup {
		\override #'(font-name . "Arial")
		\tiny \column {
			\fill-line { "Transcription" \italic { "Giuseppe Ricupero" } }
			\fill-line { "Updated" \italic { "29-08-2016 15.43" } }
		}
	}
}

global = {
	\time 4/4
	\key f \major
	\tempo 4 = 100
	\set Score.skipBars = ##t
	\set Score.countPercentRepeats = ##t
	\set Staff.hairpinToBarline = ##f
}
